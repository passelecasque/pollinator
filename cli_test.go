package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCLI(t *testing.T) {
	fmt.Println("+ Testing download...")
	check := assert.New(t)

	m := &pollinatorArgs{}
	check.Nil(m.parseCLI([]string{"show-config"}))
	check.True(m.showConfig)

	check.NotNil(m.parseCLI([]string{"blue", "purple", "h2345"}))
	check.Nil(m.parseCLI([]string{"blue", "purple", "12345"}))
	check.Equal("blue", m.trackerOrigin)
	check.Equal("purple", m.trackerDestination)
	check.Equal(12345, m.torrentID)
}
