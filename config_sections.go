package main

import (
	"strconv"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/logthis"
)

type ConfigGeneral struct {
	LogLevel int `yaml:"log_level"`
}

func (cg *ConfigGeneral) check() error {
	if cg.LogLevel < logthis.NORMAL || cg.LogLevel > logthis.VERBOSESTEST {
		return errors.New("invalid log level")
	}
	return nil
}

// String representation for ConfigGeneral.
func (cg *ConfigGeneral) String() string {
	txt := "General configuration:\n"
	txt += "\tLog level: " + strconv.Itoa(cg.LogLevel) + "\n"
	return txt
}

type ConfigTracker struct {
	Name     string
	User     string
	Password string
	Cookie   string
	URL      string
}

func (ct *ConfigTracker) check() error {
	if ct.Name == "" {
		return errors.New("Missing tracker name")
	}
	if ct.User == "" {
		return errors.New("Missing tracker username for " + ct.Name)
	}
	if ct.Cookie == "" && ct.Password == "" {
		return errors.New("Missing log in information (password or session cookie) for " + ct.Name)
	}
	if ct.URL == "" {
		return errors.New("Missing tracker URL for " + ct.Name)
	}
	return nil
}

func (ct *ConfigTracker) String() string {
	txt := "Tracker configuration for " + ct.Name + "\n"
	txt += "\tUser: " + ct.User + "\n"
	txt += "\tPassword: " + ct.Password + "\n"
	txt += "\tCookie value: " + ct.Cookie + "\n"
	txt += "\tURL: " + ct.URL + "\n"
	return txt
}

type ConfigFolders struct {
	WatchDir    string `yaml:"watch_directory"`
	DownloadDir string `yaml:"download_directory"`
}

func (ca *ConfigFolders) check() error {
	if ca.DownloadDir == "" || !fs.DirExists(ca.DownloadDir) {
		return errors.New("Downloads directory does not exist")
	}
	if ca.WatchDir == "" || !fs.DirExists(ca.WatchDir) {
		return errors.New("Watch directory does not exist")
	}
	if ca.WatchDir == ca.DownloadDir {
		return errors.New("Watch directories are generally not also download directories")
	}
	return nil
}

func (ca *ConfigFolders) String() string {
	txt := "Folders configuration\n"
	txt += "\tWatch directory: " + ca.WatchDir + "\n"
	txt += "\tDownload directory: " + ca.DownloadDir + "\n"
	return txt
}
